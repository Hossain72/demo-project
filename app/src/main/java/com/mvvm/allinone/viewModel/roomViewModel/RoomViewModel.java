package com.mvvm.allinone.viewModel.roomViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.mvvm.allinone.model.roomModel.Contact;
import com.mvvm.allinone.repository.roomRepo.RoomRepo;

import java.util.List;

public class RoomViewModel extends AndroidViewModel {

    private RoomRepo roomRepo;

    public RoomViewModel(@NonNull Application application) {
        super(application);
        roomRepo = new RoomRepo(getApplication());
    }

    public long insertContact(Contact contact) {
        return roomRepo.insertContact(contact);
    }

    public LiveData<List<Contact>> getAllContact(){
        return roomRepo.showAllContact();
    }
}
