package com.mvvm.allinone.viewModel.firebaseVIewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mvvm.allinone.model.firebaseModel.User;
import com.mvvm.allinone.repository.firebaseRepo.FirebaseRepo;

import java.util.List;

public class FirebaseViewModel extends AndroidViewModel {

    private FirebaseRepo firebaseRepo;

    public FirebaseViewModel(@NonNull Application application) {
        super(application);
        firebaseRepo = new FirebaseRepo(getApplication());
    }

    public void insertUser(User user){
        firebaseRepo.insertUser(user);
    }

    public MutableLiveData<List<User>> showUser(){

        return firebaseRepo.showUser();

    }

    public void deleteUser(String userId){
        firebaseRepo.deleteUser(userId);
    }

}
