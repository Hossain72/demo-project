package com.mvvm.allinone.viewModel.sqliteViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.mvvm.allinone.model.sqliteModel.User;
import com.mvvm.allinone.repository.sqliteRepository.SQLiteRepo;

public class SQLiteViewModel extends AndroidViewModel {

    private SQLiteRepo sqLiteRepo;

    public SQLiteViewModel(@NonNull Application application) {
        super(application);
        sqLiteRepo = new SQLiteRepo(getApplication());
    }

    public void insert(User user){
        sqLiteRepo.insert(user);
    }

}
