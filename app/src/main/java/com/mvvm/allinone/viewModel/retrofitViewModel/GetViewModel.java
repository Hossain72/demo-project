package com.mvvm.allinone.viewModel.retrofitViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.mvvm.allinone.model.retrofitModel.Post;
import com.mvvm.allinone.repository.retrofitRepo.GetRepo;

import java.util.List;

public class GetViewModel extends AndroidViewModel {

    private GetRepo getRepo;

    public GetViewModel(@NonNull Application application) {
        super(application);
        getRepo = new GetRepo();
    }

    public MutableLiveData<List<Post>> getAllPost(){
        return getRepo.getAllPost();
    }

    public MutableLiveData<Post> getPostById(int id){
        return getRepo.getPostById(id);
    }

}
