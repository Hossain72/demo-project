package com.mvvm.allinone.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ActivityFirebaseBinding;
import com.mvvm.allinone.firebase.FirebaseInterface;
import com.mvvm.allinone.model.firebaseModel.User;
import com.mvvm.allinone.view.adapter.UserFirebaseAdapter;
import com.mvvm.allinone.viewModel.firebaseVIewModel.FirebaseViewModel;

import java.util.ArrayList;
import java.util.List;

public class FirebaseActivity extends AppCompatActivity implements FirebaseInterface {

    private ActivityFirebaseBinding binding;
    private FirebaseViewModel viewModel;
    private User user;
    private List<User> userList;
    private UserFirebaseAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_firebase);

        viewModel = ViewModelProviders.of(this).get(FirebaseViewModel.class);

        userList = new ArrayList<>();
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        binding.insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.nameET.equals("")){
                    Toast.makeText(FirebaseActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                } else if (binding.genderET.equals("")){
                    Toast.makeText(FirebaseActivity.this, "Please Enter Gender", Toast.LENGTH_SHORT).show();
                } else if (binding.mobileNumber.length() != 11){
                    Toast.makeText(FirebaseActivity.this, "Please Enter Mobile Mobile Number", Toast.LENGTH_SHORT).show();
                } else {
                    String name = binding.nameET.getText().toString();
                    String gender = binding.genderET.getText().toString();
                    String mobileNumber = binding.mobileNumber.getText().toString();

                    user = new User(name, gender, mobileNumber);

                    insertUser(user);
                }
            }
        });

        showUser();

    }

    private void insertUser(User user) {
        viewModel.insertUser(user);
    }


    private void showUser() {
        viewModel.showUser().observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                userList.clear();
                userList.addAll(users);
                adapter = new UserFirebaseAdapter(userList, FirebaseActivity.this, FirebaseActivity.this);
                adapter.notifyDataSetChanged();
                binding.recyclerView.setAdapter(adapter);
            }
        });
    }


    @Override
    public void updateUser(final String userId) {

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("User");

        databaseReference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                User user = dataSnapshot.getValue(User.class);

                binding.nameET.setText(user.getName());
                binding.genderET.setText(user.getGender());
                binding.mobileNumber.setText(user.getMobileNumber());
                binding.insertBtn.setText("Update");

                binding.insertBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String name = binding.nameET.getText().toString();
                        final String gender = binding.genderET.getText().toString();
                        String mobileNumber = binding.mobileNumber.getText().toString();

                        User user = new User(userId, name, gender, mobileNumber);
                        databaseReference.child(userId).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                binding.nameET.setText("");
                                binding.genderET.setText("");
                                binding.mobileNumber.setText("");
                                binding.insertBtn.setText("Insert");
                                Toast.makeText(FirebaseActivity.this, "Update", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(FirebaseActivity.this, "" + e, Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void deleteUser(String userId) {
        viewModel.deleteUser(userId);
    }
}
