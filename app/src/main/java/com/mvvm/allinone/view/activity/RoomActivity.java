package com.mvvm.allinone.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.room.Room;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ActivityMainBinding;
import com.mvvm.allinone.databinding.ActivityRoomBinding;
import com.mvvm.allinone.model.roomModel.Contact;
import com.mvvm.allinone.room.ContactDatabase;
import com.mvvm.allinone.view.adapter.ContactRoomAdapter;
import com.mvvm.allinone.viewModel.roomViewModel.RoomViewModel;

import java.util.ArrayList;
import java.util.List;

public class RoomActivity extends AppCompatActivity {

    private ActivityRoomBinding binding;
    private Contact contact;
    private List<Contact> contactList;
    private ContactRoomAdapter adapter;
    private RoomViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_room);

        viewModel = ViewModelProviders.of(this).get(RoomViewModel.class);

        contactList = new ArrayList<>();
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        binding.insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = binding.nameET.getText().toString();
                String email = binding.emailET.getText().toString();
                String mobileNumber = binding.mobileNumberET.getText().toString();

                if (name.equals("")){
                    Toast.makeText(RoomActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
                } else if (!isValidEmail(email)){
                    Toast.makeText(RoomActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
                } else if (mobileNumber.length() != 11){
                    Toast.makeText(RoomActivity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
                }else {
                    insertContact(name, email, mobileNumber);
                }

            }
        });

        showContact();

    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void insertContact(String name, String email, String mobileNumber) {

        contact = new Contact(name, email, mobileNumber);

        long id = viewModel.insertContact(contact);
        Toast.makeText(this, "" + id, Toast.LENGTH_SHORT).show();

    }


    private void showContact() {

        if (contactList != null){

            viewModel.getAllContact().observe(this, new Observer<List<Contact>>() {
                @Override
                public void onChanged(List<Contact> contacts) {
                    contactList.clear();
                    contactList.addAll(contacts);
                    adapter = new ContactRoomAdapter(contactList, RoomActivity.this);
                    adapter.notifyDataSetChanged();
                    binding.recyclerView.setAdapter(adapter);
                }
            });


        }

    }

}
