package com.mvvm.allinone.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ItemContactBinding;
import com.mvvm.allinone.model.roomModel.Contact;
import com.mvvm.allinone.room.ContactDatabase;
import com.mvvm.allinone.viewModel.roomViewModel.RoomViewModel;

import java.util.List;

public class ContactRoomAdapter extends RecyclerView.Adapter<ContactRoomAdapter.ViewHolder> {

    private List<Contact> contactList;
    private Context context;
    private ItemContactBinding binding;
    private ContactDatabase database;

    public ContactRoomAdapter(List<Contact> contactList, Context context) {
        this.contactList = contactList;
        this.context = context;
        database = Room.databaseBuilder(context, ContactDatabase.class, "ContactDB").allowMainThreadQueries().build();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_contact, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Contact contact = contactList.get(position);
        holder.binding.nameTV.setText("Name: " + contact.getName());
        holder.binding.emailTV.setText("Email: " + contact.getEmail());
        holder.binding.mobileNumberTV.setText("Mobile NUmber: " + contact.getMobileNumber());


        holder.binding.popUpMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(context, holder.binding.popUpMenu);
                popupMenu.getMenuInflater().inflate(R.menu.pop_up_menu, popupMenu.getMenu());
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {


                        switch (item.getItemId()) {

                            case R.id.updateMenu:
                                Toast.makeText(context, "Update", Toast.LENGTH_SHORT).show();
                                break;

                            case R.id.deleteMenu:
                                database.getContactDao().deleteContact(contact);
                                contactList.remove(contact);
                                notifyDataSetChanged();
                                Toast.makeText(context, "Delete", Toast.LENGTH_SHORT).show();
                                break;

                        }

                        return false;
                    }
                });

            }
        });

    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemContactBinding binding;

        public ViewHolder(@NonNull ItemContactBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }
}
