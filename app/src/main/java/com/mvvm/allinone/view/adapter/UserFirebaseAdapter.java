package com.mvvm.allinone.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ItemUserBinding;
import com.mvvm.allinone.firebase.FirebaseInterface;
import com.mvvm.allinone.model.firebaseModel.User;

import java.util.List;

public class UserFirebaseAdapter extends RecyclerView.Adapter<UserFirebaseAdapter.ViewHolder> {

    private ItemUserBinding binding;
    private List<User> userList;
    private Context context;
    private FirebaseInterface firebaseInterface;

    public UserFirebaseAdapter(List<User> userList, Context context, FirebaseInterface firebaseInterface) {
        this.userList = userList;
        this.context = context;
        this.firebaseInterface = firebaseInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_user, parent, false);

        return new ViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final User currentUser = userList.get(position);
        holder.binding.nameTV.setText("Name: " + currentUser.getName());
        holder.binding.genderTV.setText("Gender: " + currentUser.getGender());
        holder.binding.mobileNumberTV.setText("Number: " + currentUser.getMobileNumber());

        holder.binding.popUpMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(context, holder.binding.popUpMenu);
                popupMenu.getMenuInflater().inflate(R.menu.pop_up_menu, popupMenu.getMenu());
                popupMenu.show();

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        String userId = currentUser.getUserID();

                        switch (item.getItemId()) {

                            case R.id.updateMenu:
                                firebaseInterface.updateUser(userId);
                                break;

                            case R.id.deleteMenu:
                                firebaseInterface.deleteUser(userId);
                                break;

                        }

                        return false;
                    }
                });


            }
        });

    }

    @Override
    public int getItemCount() {

        if (userList != null){
            return userList.size();
        }

        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ItemUserBinding binding;

        public ViewHolder(@NonNull ItemUserBinding binding) {
            super(binding.getRoot());

            this.binding = binding;

        }
    }
}
