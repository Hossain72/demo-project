package com.mvvm.allinone.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding activityMainBinding;
    private MainActivityClickHandlers mainActivityClickHandlers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mainActivityClickHandlers = new MainActivityClickHandlers(this);
        activityMainBinding.setClickHandle(mainActivityClickHandlers);

    }

    public class MainActivityClickHandlers{

        private Context context;

        public MainActivityClickHandlers(Context context) {
            this.context = context;
        }

        public void onSQLiteClicked(View view){
            startActivity(new Intent(context, SqliteActivity.class));;
        }

        public void onFirebaseClicked(View view){
            startActivity(new Intent(context, FirebaseActivity.class));
        }

        public void onRetrifitClicked(View view){
            startActivity(new Intent(context, RetrofitActivity.class));
        }

        public void onGoogleMapClicked(View view){
            startActivity(new Intent(context, MapActivity.class));
        }

        public void onRoomClicked(View view){
            startActivity(new Intent(MainActivity.this, RoomActivity.class));
        }

    }

}
