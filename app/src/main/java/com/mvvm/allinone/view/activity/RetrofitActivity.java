package com.mvvm.allinone.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ActivityRetrofitBinding;
import com.mvvm.allinone.model.retrofitModel.Post;
import com.mvvm.allinone.view.adapter.RetrofitViewPagerAdapter;
import com.mvvm.allinone.view.fragment.AllPostFragment;
import com.mvvm.allinone.view.fragment.PostByIdFragment;
import com.mvvm.allinone.viewModel.retrofitViewModel.GetViewModel;

import java.util.ArrayList;
import java.util.List;

public class RetrofitActivity extends AppCompatActivity {

    private ActivityRetrofitBinding activityRetrofitBinding;
    private List<Fragment> fragmentList;
    private List<String> titleList;
    private GetViewModel getViewModel;
    private RetrofitViewPagerAdapter retrofitViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityRetrofitBinding = DataBindingUtil.setContentView(this, R.layout.activity_retrofit);

        activityRetrofitBinding.tabLayout.setupWithViewPager(activityRetrofitBinding.viewPager);

        fragmentList = new ArrayList<>();
        fragmentList.add(new AllPostFragment());
        fragmentList.add(new PostByIdFragment());

        titleList = new ArrayList<>();
        titleList.add("All Post");
        titleList.add("Post By ID");

        retrofitViewPagerAdapter = new RetrofitViewPagerAdapter(getSupportFragmentManager(), fragmentList, titleList);
        activityRetrofitBinding.viewPager.setAdapter(retrofitViewPagerAdapter);

    }

}
