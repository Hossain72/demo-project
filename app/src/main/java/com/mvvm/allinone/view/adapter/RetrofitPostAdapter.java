package com.mvvm.allinone.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mvvm.allinone.R;
import com.mvvm.allinone.model.retrofitModel.Post;

import java.util.List;

public class RetrofitPostAdapter extends RecyclerView.Adapter<RetrofitPostAdapter.ViewHolder> {

    private List<Post> postList;

    public RetrofitPostAdapter(List<Post> postList) {
        this.postList = postList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Post post = postList.get(position);
        holder.userIdTV.setText("User ID: " + String.valueOf(post.getUserId()));
        holder.idTV.setText("Post ID: " + String.valueOf(post.getId()));
        holder.titleTV.setText("Title: " + post.getTitle());
        holder.bodyTV.setText("Body: " + post.getBody());

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userIdTV, idTV, titleTV, bodyTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userIdTV = itemView.findViewById(R.id.userIdTV);
            idTV = itemView.findViewById(R.id.idTV);
            titleTV = itemView.findViewById(R.id.titleTV);
            bodyTV = itemView.findViewById(R.id.bodyTV);
        }
    }
}
