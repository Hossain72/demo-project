package com.mvvm.allinone.view.fragment;


import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mvvm.allinone.R;
import com.mvvm.allinone.model.retrofitModel.Post;
import com.mvvm.allinone.viewModel.retrofitViewModel.GetViewModel;

public class PostByIdFragment extends Fragment {

    private CardView cardView;
    private EditText idET;
    private Button submitBtn;
    private TextView titleTV, bodyTV;
    private GetViewModel getViewModel;

    public PostByIdFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_by_id, container, false);

        idET = view.findViewById(R.id.idET);
        submitBtn = view.findViewById(R.id.submitBtn);
        titleTV = view.findViewById(R.id.titleTV);
        bodyTV = view.findViewById(R.id.bodyTV);
        cardView = view.findViewById(R.id.cardView);

        getViewModel = ViewModelProviders.of(this).get(GetViewModel.class);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = idET.getText().toString();
                if (id.equals("")) {
                    Toast.makeText(getContext(), "Please enter id 1 to 100", Toast.LENGTH_SHORT).show();
                } else if (Integer.valueOf(id) < 0) {
                    Toast.makeText(getContext(), "Please enter id 1 to 100", Toast.LENGTH_SHORT).show();
                } else if (Integer.valueOf(id) > 100) {
                    Toast.makeText(getContext(), "Please enter id 1 to 100", Toast.LENGTH_SHORT).show();
                } else {
                    getPostById(id);
                }

            }
        });

        return view;
    }

    private void getPostById(String id) {

        getViewModel.getPostById(Integer.valueOf(id)).observe(this, new Observer<Post>() {
            @Override
            public void onChanged(Post post) {
                Post post1 = post;

                cardView.setVisibility(View.VISIBLE);

                titleTV.setText("Title: " + post1.getTitle());
                bodyTV.setText("Body: " + post1.getBody());
            }
        });

    }

}
