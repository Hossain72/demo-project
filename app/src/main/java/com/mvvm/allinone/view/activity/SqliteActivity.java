package com.mvvm.allinone.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.mvvm.allinone.R;
import com.mvvm.allinone.databinding.ActivitySqliteBinding;
import com.mvvm.allinone.model.sqliteModel.User;
import com.mvvm.allinone.viewModel.sqliteViewModel.SQLiteViewModel;

public class SqliteActivity extends AppCompatActivity {

    private ActivitySqliteBinding activitySqliteBinding;
    private User user;
    private SQLiteViewModel sqLiteViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activitySqliteBinding = DataBindingUtil.setContentView(this, R.layout.activity_sqlite);

        sqLiteViewModel = ViewModelProviders.of(this).get(SQLiteViewModel.class);

        activitySqliteBinding.insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = activitySqliteBinding.nameET.getText().toString();

                user = new User(name);

                sqLiteViewModel.insert(user);

                Toast.makeText(SqliteActivity.this, "Successful", Toast.LENGTH_SHORT).show();

            }
        });

    }

}
