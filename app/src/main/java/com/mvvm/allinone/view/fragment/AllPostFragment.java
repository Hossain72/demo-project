package com.mvvm.allinone.view.fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mvvm.allinone.R;
import com.mvvm.allinone.model.retrofitModel.Post;
import com.mvvm.allinone.view.adapter.RetrofitPostAdapter;
import com.mvvm.allinone.viewModel.retrofitViewModel.GetViewModel;

import java.util.ArrayList;
import java.util.List;

public class AllPostFragment extends Fragment {

    private RecyclerView recyclerView;
    private GetViewModel getViewModel;
    private List<Post> postList;
    private RetrofitPostAdapter retrofitPostAdapter;

    public AllPostFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view =  inflater.inflate(R.layout.fragment_all_post, container, false);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        postList = new ArrayList<>();
        getViewModel = ViewModelProviders.of(this).get(GetViewModel.class);

        getAllPost();

        return view;

    }

    private void getAllPost() {

        getViewModel.getAllPost().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(List<Post> posts) {
                postList.addAll(posts);
                retrofitPostAdapter = new RetrofitPostAdapter(postList);
                recyclerView.setAdapter(retrofitPostAdapter);
            }
        });

    }

}
