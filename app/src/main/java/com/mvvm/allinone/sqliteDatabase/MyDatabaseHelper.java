package com.mvvm.allinone.sqliteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.mvvm.allinone.model.sqliteModel.User;

public class MyDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "InformationDB.db";
    private static final int DB_VERSION = 2;
    private Context context;
    private SQLiteDatabase sqLiteDatabase;

    private static final String USER_TABLE = "user_table";
    private static final String ID = "_id";
    private static final String NAME = "name";

    private static final String CREATE_USER_TABLE = "create table " + USER_TABLE + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + NAME + " VARCHAR(30));";
    private static final String DROP_TABLE = "drop table if exists " + USER_TABLE;

    private static final String DISPLAY_ALL = "select * from " + USER_TABLE;


    public MyDatabaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        try {
            db.execSQL(CREATE_USER_TABLE);
            Toast.makeText(context, "onCreate called", Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(context, "Error" + e, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            db.execSQL(CREATE_USER_TABLE);
            Toast.makeText(context, "onUpgrade called", Toast.LENGTH_LONG).show();
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }catch (Exception e){
            Toast.makeText(context, "Error" + e, Toast.LENGTH_LONG).show();
        }

    }

    public SQLiteDatabase getInstance(){

        if (this.getWritableDatabase() == null){
            sqLiteDatabase = this.getWritableDatabase();
        }

        return sqLiteDatabase;
    }

    public long insert(User user){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME, user.getName());

        return sqLiteDatabase.insert(USER_TABLE, null, contentValues);

    }

    public Cursor showData(){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(DISPLAY_ALL, null);

        return cursor;

    }


}
