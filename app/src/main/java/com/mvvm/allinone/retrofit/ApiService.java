package com.mvvm.allinone.retrofit;

import com.mvvm.allinone.model.retrofitModel.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("posts")
    Call<List<Post>> getAllPost();

    @GET("posts/{id}")
    Call<Post> getPostById(@Path("id") int id);


}
