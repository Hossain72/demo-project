package com.mvvm.allinone.firebase;

public interface FirebaseInterface {

    void updateUser(String userId);

    void deleteUser(String userId);

}
