package com.mvvm.allinone.model.firebaseModel;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

public class User {

    private String userID;
    private String name;
    private String gender;
    private String mobileNumber;

    public User() {
    }

    public User(String name, String gender, String mobileNumber) {
        this.name = name;
        this.gender = gender;
        this.mobileNumber = mobileNumber;
    }

    public User(String userID, String name, String gender, String mobileNumber) {
        this.userID = userID;
        this.name = name;
        this.gender = gender;
        this.mobileNumber = mobileNumber;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}

