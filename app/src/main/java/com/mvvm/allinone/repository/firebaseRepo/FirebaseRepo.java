package com.mvvm.allinone.repository.firebaseRepo;

import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mvvm.allinone.firebase.FirebaseInterface;
import com.mvvm.allinone.model.firebaseModel.User;

import java.util.ArrayList;
import java.util.List;

public class FirebaseRepo {

    private Context context;
    private DatabaseReference databaseReference;
    private String userId;
    private List<User> userList;

    public FirebaseRepo(Context context) {
        this.context = context;
    }

    public void insertUser(User user){

        databaseReference = FirebaseDatabase.getInstance().getReference().child("User");
        userId = databaseReference.push().getKey();
        user.setUserID(userId);

        databaseReference.child(userId).setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()){
                    Toast.makeText(context, "Successfully insert", Toast.LENGTH_LONG).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "" + e, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public MutableLiveData<List<User>> showUser(){

        final MutableLiveData<List<User>> mutableLiveData = new MutableLiveData<>();
        userList = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("User");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){
                    userList.clear();
                    for (DataSnapshot data : dataSnapshot.getChildren()){
                        userId = data.getKey();
                        User user = data.getValue(User.class);
                        userList.add(user);
                    }
                    mutableLiveData.setValue(userList);

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "" + databaseError, Toast.LENGTH_SHORT).show();
            }
        });


        return mutableLiveData;

    }

    public void deleteUser(String userId){
        databaseReference = FirebaseDatabase.getInstance().getReference().child("User");
        databaseReference.child(userId).removeValue();
        Toast.makeText(context, "Delete User", Toast.LENGTH_SHORT).show();
    }

}
