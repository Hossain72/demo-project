package com.mvvm.allinone.repository.sqliteRepository;

import android.content.Context;

import com.mvvm.allinone.model.sqliteModel.User;
import com.mvvm.allinone.sqliteDatabase.MyDatabaseHelper;

public class SQLiteRepo {

    private MyDatabaseHelper myDatabaseHelper;

    public SQLiteRepo(Context context) {
        myDatabaseHelper = new MyDatabaseHelper(context);
        myDatabaseHelper.getInstance();
    }

    public void insert(User user){
        myDatabaseHelper.insert(user);
    }

}
