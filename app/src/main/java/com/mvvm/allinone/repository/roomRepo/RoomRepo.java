package com.mvvm.allinone.repository.roomRepo;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.mvvm.allinone.model.roomModel.Contact;
import com.mvvm.allinone.room.ContactDatabase;
import com.mvvm.allinone.view.adapter.ContactRoomAdapter;

import java.util.List;

public class RoomRepo {

    private ContactDatabase database;
    private Context context;

    public RoomRepo(Context context) {
        this.context = context;
        database = ContactDatabase.getInstance(context);
    }

    public long insertContact(Contact contact){
        return database.getContactDao().insertContact(contact);
    }

    public LiveData<List<Contact>> showAllContact(){
        return database.getContactDao().showAllContact();
    }

}
