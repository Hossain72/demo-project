package com.mvvm.allinone.repository.retrofitRepo;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mvvm.allinone.model.retrofitModel.Post;
import com.mvvm.allinone.retrofit.ApiService;
import com.mvvm.allinone.retrofit.RetrofitInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.POST;

public class GetRepo {

    private static String TAG = "RETROFIT";
    private ApiService apiService;

    public GetRepo() {
        apiService = RetrofitInstance.getInstance().create(ApiService.class);
    }

    public MutableLiveData<List<Post>> getAllPost(){

        final MutableLiveData<List<Post>> mutableLiveData = new MutableLiveData<>();

        apiService.getAllPost().enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {

                if (response != null){
                    mutableLiveData.postValue(response.body());
                }

            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });

        return mutableLiveData;

    }

    public MutableLiveData<Post> getPostById(int id){

        final MutableLiveData<Post> mutableLiveData = new MutableLiveData<>();

        apiService.getPostById(id).enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {

                mutableLiveData.postValue(response.body());

            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });

        return mutableLiveData;
    }

}
