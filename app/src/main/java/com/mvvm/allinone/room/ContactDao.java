package com.mvvm.allinone.room;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.mvvm.allinone.model.roomModel.Contact;

import java.util.List;

@Dao
public interface ContactDao {

    @Insert
    long insertContact(Contact contact);

    @Update
    void updateContact(Contact contact);

    @Delete
    void deleteContact(Contact contact);

    @Query("select * from contact")
    LiveData<List<Contact>> showAllContact();

}
