package com.mvvm.allinone.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mvvm.allinone.model.roomModel.Contact;

@Database(entities = {Contact.class}, version = 1)
public abstract class ContactDatabase extends RoomDatabase {

    private static ContactDatabase instance;

    public static synchronized ContactDatabase getInstance(Context context){

        if (instance == null){

            instance = Room.databaseBuilder(context,ContactDatabase.class, "ContactDB").allowMainThreadQueries().build();

        }

        return instance;
    }

    public abstract ContactDao  getContactDao();

}
